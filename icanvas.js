// iCanvas
// A simple image viewer using HTML5 Canvas
// version 0.1, November 1, 2013
// by João Marques

(function($) {
    
    $.iCanvas = function(element, options) {
		
        var DEFAULT_ANGLE = 0;                
        var MINIMUM_SCALE_FACTOR = 1;
        var MAXIMUM_SCALE_FACTOR = 6;
        var ZOOM_WINDOW_DIV = 'zoomWindow';
        var ZOOM_WINDOW_CANVAS = 'zoomCanvasWindow';

        var imgCanvas;
        var originalSize = {};          
        var maximumScale = MINIMUM_SCALE_FACTOR;        
        var current = { 
        	image : null, 
        	angle : DEFAULT_ANGLE, 
        	scale : MINIMUM_SCALE_FACTOR        	
        };                

        var defaults = {
            width: 600,
            height: 560,
            zoomWindow: true,
            zoomWidth: 200,
            zoomHeight: 100,
            canvasImgId: 'imgViewerCanvas', 
        	rotateRightBtn: 'rotateRight',
        	rotateLeftBtn: 'rotateLeft',
        	zoomInBtn: 'zoomIn',
        	zoomOutBtn: 'zoomOut'        	
        }
        
        var plugin = this;
        
        plugin.settings = {}

        var $element = $(element), // reference to the jQuery version of DOM element
             element = element;    // reference to the actual DOM element
        
        plugin.init = function() {
            
            plugin.settings = $.extend({}, defaults, options);
            
            jQuery('<canvas/>', {
    			id: plugin.settings.canvasImgId    			
			}).appendTo(element);                	

            if(plugin.settings.zoomWindow) {
            	var zoomWindow = jQuery('<div/>', {
	    			id: ZOOM_WINDOW_DIV,
	    			width: plugin.settings.zoomWidth,
	    			height: plugin.settings.zoomHeight,
	    			style: 'display: none;'
				}).appendTo(element);                	

				var zoomCanvas = jQuery('<canvas/>', {	
	    			id: ZOOM_WINDOW_CANVAS,
	    			style: 'border-style: dashed;'	
				}).appendTo(zoomWindow);                	

				zoomCanvas.width(plugin.settings.zoomWidth);
				zoomCanvas.height(plugin.settings.zoomHeight);
            }
			
			imgCanvas = document.getElementById(plugin.settings.canvasImgId);

			// setup events
			$('#'+ plugin.settings.rotateRightBtn).click(function() {
				if(current.image){
					rotate(90);
					drawImage(current.image);	
				}				
			});	

			$('#'+ plugin.settings.rotateLeftBtn).click(function() {
				if(current.image) {
					rotate(-90);
					drawImage(current.image);
				}				
			});	

			$('#'+ plugin.settings.zoomInBtn).click(function() {
				if(current.image) {
					scale(2);						
					drawImage(current.image);
				}				
			});	

			$('#'+ plugin.settings.zoomOutBtn).click(function() {
				if(current.image) {
					scale(0.5);
					drawImage(current.image);
				}				
			});
        }        
        
        plugin.loadImage = function(imgUrl) {        				        	
    		current.angle = DEFAULT_ANGLE;
        	current.scale = MINIMUM_SCALE_FACTOR;
			current.image = new Image();																						

            var ctx = imgCanvas.getContext("2d");

            current.image.onload = function () {
            	// store original size
            	originalSize = { width: current.image.width, height: current.image.height };
            	imgCanvas.width = plugin.settings.width;
				imgCanvas.height = plugin.settings.height;
            	current.image.width = plugin.settings.width;
            	current.image.height = plugin.settings.height;

            	ctx.clearRect(0, 0, imgCanvas.width, imgCanvas.height); //clear the canvas
        		ctx.drawImage(current.image, 0, 0, plugin.settings.width, plugin.settings.height); //draw the image ;)	
				maximumScale = 	Math.ceil(originalSize.width / plugin.settings.width);				
            }
            current.image.src = imgUrl;
        	
        	if(plugin.settings.zoomWindow) {
        		setupZoom();
        	}            

			return this;
        }       

        // private methods
        var drawImage = function(img) {
                	
			var ctx = imgCanvas.getContext("2d");	

			ctx.save(); //saves the state of canvas						
            ctx.clearRect(0, 0, imgCanvas.width, imgCanvas.height); //clear the canvas
            ctx.translate(imgCanvas.width / 2, imgCanvas.height / 2); //let's translate            
            ctx.rotate(Math.PI / 180 * (current.angle)); //increment the angle and rotate the image                                     
            ctx.scale(current.scale, current.scale);
            alert(current.image.width);
            ctx.drawImage(img, -img.width / 2, -img.height / 2, current.image.width, current.image.height); //draw the image ;)
            ctx.restore(); //restore the state of canvas                       
        }            

        var setupZoom = function(){
        	$element.mousedown(function(e) {
		        var x = e.clientX;
		        var y = e.clientY;
		        var minMovement = 3;			        

	        	/*$("#" + ZOOM_WINDOW_DIV).show().css({ 
            		position: 'absolute',
            		left:  e.pageX - (plugin.settings.zoomWidth / 2),
            		top: e.pageY - (plugin.settings.zoomHeight / 2) });
            							
				drawZoom(e.pageX, e.pageY);

		        $element.mousemove(function(ev) {	
		            if (Math.abs(ev.clientX - x) > minMovement || Math.abs(ev.clientY - y) > minMovement) {		            			            			            	
		            	
		            }		            
		        });*/
		    }).mouseup(function() {
		        $element.unbind("mousemove");
		        $("#" + ZOOM_WINDOW_DIV).hide();
		    });	
        }

        var rotate = function(angle){
			current.angle += angle;					
			if(current.angle > 270 || current.angle < -270) current.angle = 0;
		}

		var scale = function(factor) {			
			current.scale = Math.floor(current.scale * factor);
			
			if(current.scale < MINIMUM_SCALE_FACTOR){
				current.scale = MINIMUM_SCALE_FACTOR;
				return;
			} 

			if(current.scale > maximumScale){				
				current.scale = maximumScale;																
				return;
			}			

			imgCanvas.width *= factor;
			imgCanvas.height *= factor;		
		}
        
        var drawZoom = function(posX, posY) {
			var canvasZoom=document.getElementById(ZOOM_WINDOW_CANVAS);			

			var ctx1=canvasZoom.getContext("2d");
			ctx1.save();					
    		ctx1.clearRect(0, 0, canvasZoom.width, canvasZoom.height); 	 
    		ctx1.scale(MAXIMUM_SCALE_FACTOR, MAXIMUM_SCALE_FACTOR);
    		ctx1.rotate(Math.PI / 180 * (current.angle)); //increment the angle and rotate the image    		
    		ctx1.translate(- posX + 50 , -posY + 20 );
    		ctx1.drawImage(current.image, 0, 0); //draw the image ;)            				                
    		ctx1.restore();
        }

        plugin.init();

    }

    // add the plugin to the jQuery.fn object
    $.fn.iCanvas = function(options) {    	        
		return new $.iCanvas(this, options);
    }

})(jQuery);

